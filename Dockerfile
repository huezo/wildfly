# Use latest jboss/base-jdk:11 image as the base
#FROM jboss/base-jdk:11
FROM  registry.gitlab.com/huezo/oracle-jdk:jdk-17

# File Author / Maintainer
MAINTAINER huezo <huezohuezo.1990@gmail.com>


WORKDIR /opt/jboss/wildflyv

RUN apt update && apt install -y curl wget nano 

# Set the WILDFLY_VERSION env variable
ENV WILDFLY_VERSION 26.1.1.Final
ENV WILDFLY_SHA1 c11076dd0ea3bb554c5336eeafdfcee18d94551d
ENV JBOSS_HOME /opt/jboss/wildfly

ENV USER_WILDFLY=vhuezo
ENV PASS_WILDFLY=vhuezo

USER root

# Add the WildFly distribution to /opt, and make wildfly the owner of the extracted tar content
# Make sure the distribution is available from a well-known place
RUN cd $HOME \
    && curl -L -O https://github.com/wildfly/wildfly/releases/download/$WILDFLY_VERSION/wildfly-$WILDFLY_VERSION.tar.gz \
    && sha1sum wildfly-$WILDFLY_VERSION.tar.gz | grep $WILDFLY_SHA1 \
    && tar xf wildfly-$WILDFLY_VERSION.tar.gz \
    && mv $HOME/wildfly-$WILDFLY_VERSION $JBOSS_HOME \
    && rm wildfly-$WILDFLY_VERSION.tar.gz \
    && chown -R root:0 ${JBOSS_HOME} \
    && chmod -R g+rw ${JBOSS_HOME}

# Ensure signals are forwarded to the JVM process correctly for graceful shutdown
ENV LAUNCH_JBOSS_IN_BACKGROUND true

USER root

RUN /opt/jboss/wildfly/bin/add-user.sh ${USER_WILDFLY} ${PASS_WILDFLY} --silent

WORKDIR /opt/jboss/wildfly/standalone/deployments/

# Expose the ports in which we're interested
EXPOSE 8080 9990

# Set the default command to run on boot
# This will boot WildFly in standalone mode and bind to all interfaces
#CMD ["/opt/jboss/wildfly/bin/standalone.sh", "-b", "0.0.0.0"]

CMD ["/opt/jboss/wildfly/bin/standalone.sh", "-b", "0.0.0.0", "-bmanagement", "0.0.0.0"]

